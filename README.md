# Running the application

1. Add `.env` file to the root directory (if missing), with following structure:

```
PORT=YOUR_PORT_NUMBER
```

2. Run `npm i` to install all required depdendencies.
3. Run `docker-compose up` to start DB dependencies.
4. Run `npm run build && npm run start` or `npm run dev` to build/start an app or run it in development mode (respectively).

## Debugging

There is VSC launch configuration available in the repository. In order to run the app using VSC debugging, run `docker-compose up` and then start `[Treeline]: Debug API` VSC configuration. 

## Building docker image

There is Dockerfile ready to build production image from. Make sure You are in the root directory, and then run `docker build -t treeline-backend .` to build the image.
You can run `docker run -p 4000:4000 treeline-backend` in order to run the container on your machine.

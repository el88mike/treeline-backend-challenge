#build stage
FROM node:16-alpine AS builder

WORKDIR /app

COPY package*.json tsconfig.json ./

RUN npm i

COPY ./src ./src

RUN npm run build

# Final stage

FROM node:16-alpine 

WORKDIR /app

COPY package.json tsconfig.json ./

RUN npm i

COPY --from=builder /app/dist ./dist

ENV PORT=4000

EXPOSE 4000

CMD npm start

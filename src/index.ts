import { ConfigService } from './config';
import { App } from './app';


const app = new App();

try {
  const configService = new ConfigService();
  
  const config = configService.createConfig();

  app.run(config.port);
} catch (error) {
  console.error(error);
  console.log('Exiting...');

  process.exit();
}

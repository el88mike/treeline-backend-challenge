import { Error } from 'mongoose';

import { Dictionary } from '../common/models';

export class ValidationError extends Error {
  errors: Dictionary;
}

export const isValidationError = (candidate: any): candidate is ValidationError => {
  return !!(candidate?.errors);
}

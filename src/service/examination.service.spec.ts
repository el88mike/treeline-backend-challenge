import {
  ExaminationModel,
  ExaminationOutcome,
} from '../model';

import {
  DbMock,
  testExaminationPayload
} from '../common/testing';

import { ExaminationService } from './examination.service';


describe('ExaminationService', () => {
  const db = new DbMock();

  let service: ExaminationService;

  beforeEach(async () => {
    await db.connect();

    service = new ExaminationService();
  });

  afterEach(async () => await db.clear());
  afterAll(async () => await db.close());

  describe('saveExamination()', () => {
    it('should run .save() method on model and return result promise', done => {
      const result = service.saveExamination(testExaminationPayload);
  
      result
        .then(examination => {
          expect(examination).toBeTruthy();
          expect(examination.outcome).toEqual(ExaminationOutcome.NEGATIVE);
          done();
        })
        .catch(err => {
          expect(err?.errors).toBeTruthy();
          done();
        });
  
      done();
    });
  });
});

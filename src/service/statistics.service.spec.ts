import {
  DbMock,
  saveExamination,
} from '../common/testing';

import { StatisticsService } from './statistics.service';

const testDay = new Date('2021-07-05T20:40:25Z');

describe('StatisticsService', () => {
  const db = new DbMock();

  let service: StatisticsService;

  beforeEach(async () => {
    await db.connect();

    service = new StatisticsService();
  });

  afterEach(async () => await db.clear());
  afterAll(async () => await db.close());

  describe('getAllByDay', () => {
    it('should return exams by given day', async done => {

      Promise.all([
        saveExamination(),
        saveExamination(),
      ]);

      service.getAllByDay(testDay)
        .then(examinations => {
          expect(examinations.length).toEqual(2);
          done();
        })
        .catch(() => {
          done();
        });
    });
  });

});

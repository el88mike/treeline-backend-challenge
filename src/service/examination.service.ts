import {
  Examination,
  ExaminationModel,
  PatientModel,
  LocationModel,
} from '../model';

export class ExaminationService {
  public saveExamination(payload: Partial<Examination>) {
    const locationPayload = payload.location;
    const patientPayload = payload.patient;

    const location = new LocationModel({ ...locationPayload });
    const patient = new PatientModel({ ...patientPayload });

    const examination = new ExaminationModel({
      outcome: payload.outcome,
      date: payload.date,
      location,
      patient,
    });

    return Promise.all([
      location.save(),
      patient.save(),
    ]).then(() => examination.save());
  }
}

import {
  startOfDay,
  endOfDay,
} from 'date-fns';

import {
  ExaminationModel,
  ExaminationOutcome,
  LocationModel,
} from '../model';

export class StatisticsService {
  public getAllByDay(day: Date) {
    return ExaminationModel.find({
      date: {
        $gte: startOfDay(day),
        $lte: endOfDay(day),
      }
    }).exec();
  }

  public getPositiveByDay(day: Date) {
    return ExaminationModel.find({
      date: {
        $gte: startOfDay(day),
        $lte: endOfDay(day),
      },
      outcome: ExaminationOutcome.POSITIVE,
    }).exec();
  }

  public getAllByCountry(country: string) {
    const examinationsQuery = ExaminationModel.find();
  
    return LocationModel
      .find({ country })
      .exec()
      .then(
        locations => examinationsQuery.where({ location: { $in: locations } }).exec(),
      );
  }
}

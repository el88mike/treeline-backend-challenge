import { connect } from 'mongoose';

import mongoose from 'mongoose';

export class Database {
  public static init() {
    connect('mongodb://treeline:treeline@localhost:27017/jshiring', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }
}

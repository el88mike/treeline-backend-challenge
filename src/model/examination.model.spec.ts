import { Types } from 'mongoose';

import {
  ExaminationModel,
  ExaminationOutcome,
} from './examination.model';

const testId = 'testtesttest';

describe('ExaminationModel', () => {
  it('should fail when any field is empty', done => {
    const m = new ExaminationModel();

    m.validate(err => {
      expect(err?.errors).toBeTruthy();
      done();
    });
  });

  it('should fail when outcome is not a correct enum value', done => {
    const m = new ExaminationModel({
      outcome: 'INCORRECT_VALUE'
    });

    m.validate(err => {
      expect(err?.errors).toBeTruthy();
      done();
    });
  });

  it('should pass when model data is correct', done => {
    const m = new ExaminationModel({
      outcome: ExaminationOutcome.NEGATIVE,
      location: new Types.ObjectId(testId),
      patient: new Types.ObjectId(testId),
      date: new Date(),
    });

    m.validate(err => {
      expect(err?.errors).toBeFalsy();
      done();
    });

    expect(m.outcome).toEqual(ExaminationOutcome.NEGATIVE);
    expect(m.location).toBeDefined();
    expect(m.patient).toBeDefined();
  });
});

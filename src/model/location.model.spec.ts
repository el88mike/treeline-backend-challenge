import {
  LocationModel,
} from './location.model';

const testLatitude = 100;
const testLongitude = 200;

describe('LocationModel', () => {
  it('should fail when any field is empty', done => {
    const m = new LocationModel();

    m.validate(err => {
      expect(err?.errors).toBeTruthy();
      done();
    });
  });

  it('should pass when model data is correct', done => {
    const m = new LocationModel({
      latitude: testLatitude,
      longitude: testLongitude,
      country: 'poland'
    });

    m.validate(err => {
      expect(err?.errors).toBeFalsy();
      done();
    });

    expect(m.latitude).toEqual(testLatitude);
    expect(m.longitude).toEqual(testLongitude);
  });
});

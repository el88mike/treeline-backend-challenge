import {
  Document,
  Schema,
  model,
} from 'mongoose';

export const LOCATION_MODEL_NAME = 'Location';

export interface Location {
  latitude: number;
  longitude: number;
  country: string;
}

export type LocationDocument = 
  & Location
  & Document;

export const LocationSchema = new Schema<Location>({
  latitude: {
    type: Number,
    required: true,
  },
  longitude: {
    type: Number,
    required: true,
  },
  country: {
    type: String,
    required: true,
  }
});

export const LocationModel = model<LocationDocument>(LOCATION_MODEL_NAME, LocationSchema);

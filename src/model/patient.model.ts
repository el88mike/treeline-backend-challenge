import {
  Document,
  Schema,
  model,
} from 'mongoose';

export const PATIENT_MODEL_NAME = 'Patient';


export interface Patient {
  firstName: string;
  lastName: string;
  age: number;
}

export type PatientDocument =
  & Patient
  & Document;

export const PatientSchema = new Schema<Patient>({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
});

export const PatientModel = model<PatientDocument>(PATIENT_MODEL_NAME, PatientSchema);

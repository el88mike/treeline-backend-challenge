import {
  PatientModel,
} from './patient.model';

const testFirstName = 'John';
const testLastName = 'Doe';
const testAge = 25;

describe('PatientModel', () => {
  it('should fail when any field is empty', done => {
    const m = new PatientModel();

    m.validate(err => {
      expect(err?.errors).toBeTruthy();
      done();
    });
  });

  it('should pass when model data is correct', done => {
    const m = new PatientModel({
      firstName: testFirstName,
      lastName: testLastName,
      age: testAge,
    });

    m.validate(err => {
      expect(err?.errors).toBeFalsy();
      done();
    });

    expect(m.firstName).toEqual(testFirstName);
    expect(m.lastName).toEqual(testLastName);
    expect(m.age).toEqual(testAge);
  });
});

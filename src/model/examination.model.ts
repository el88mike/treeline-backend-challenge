import {
  Document,
  Schema,
  model,
} from 'mongoose';

import {
  Location,
  LOCATION_MODEL_NAME,
} from './location.model';

import {
  Patient,
  PATIENT_MODEL_NAME,
} from './patient.model';

export const EXAMINATION_MODEL_NAME = 'Examination';

export enum ExaminationOutcome {
  POSITIVE = 'POSITIVE',
  NEGATIVE = 'NEGATIVE'
}

export interface Examination {
  outcome: ExaminationOutcome;
  location: Location;
  patient: Patient;
  date: Date;
}

export type ExaminationDocument = 
  & Examination
  & Document;

export const ExaminationSchema = new Schema<Examination>({
  outcome: {
    type: String,
    enum: [
      ExaminationOutcome.POSITIVE,
      ExaminationOutcome.NEGATIVE,
    ],
    required: true,
  },
  location: {
    type: Schema.Types.ObjectId,
    ref: LOCATION_MODEL_NAME,
    required: true,
  },
  patient: {
    type: Schema.Types.ObjectId,
    ref: PATIENT_MODEL_NAME,
    required: true,
  },
  date: {
    type: Date,
    required: true,
  },
});

export const ExaminationModel = model<ExaminationDocument>(EXAMINATION_MODEL_NAME, ExaminationSchema);

import express from 'express';
import bodyParser from 'body-parser';

import { Database } from './model/db';

import {
  HelloController,
  ExaminationController,
  StatisticsController,
} from './controller';


export class App {
  private _instance: express.Express;

  public get instance() {
    return this._instance;
  }

  public run(port: number) {
    Database.init();
  
    this._instance = this._createRouter();
    return this._instance.listen(port, () => console.log(`App listening on :${port}`));
  }

  private _createRouter() {
    const helloController = new HelloController();
    const examinationController = new ExaminationController();
    const statisticsController = new StatisticsController();

    const app = express();

    app.use(bodyParser.json());

    app.use('/', helloController.getRouter());
    app.use('/examinations', examinationController.getRouter());
    app.use('/statistics', statisticsController.getRouter());

    return app;
  }
}

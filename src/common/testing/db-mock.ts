import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

export class DbMock {
  private _mongod: MongoMemoryServer;

  public async connect() {
    this._mongod = await MongoMemoryServer.create();
  
    const uri = this._mongod.getUri();

    const mongooseOpts = {
        useNewUrlParser: true,
        autoReconnect: true,
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000
    };

    await mongoose.connect(uri, mongooseOpts);
  }

  public async close() {
    await mongoose.connection.dropDatabase();
    await mongoose.connection.close();
    await this._mongod.stop();
  }

  public async clear() {
    const collections = mongoose.connection.collections;

    for (const key in collections) {
        const collection = collections[key];
        await collection.deleteMany({});
    }
  }
}

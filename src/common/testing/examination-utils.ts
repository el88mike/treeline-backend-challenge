import {
  Examination,
  ExaminationModel,
  ExaminationOutcome,
  LocationModel,
  PatientModel,
} from '../../model';

export const testExaminationPayload = {
  outcome: ExaminationOutcome.NEGATIVE,
  location: {
      latitude: 1000,
      longitude: 200,
      country: 'poland'
  },
  patient: {
      firstName: 'John',
      lastName: 'Doe',
      age: 25
  },
  date: new Date('2021-07-05T20:40:25Z')
};

export const saveExamination = (override: Partial<Examination> = {}) => {
  const baseExamination = {
    ...testExaminationPayload,
    ...override,
  };

  const location = new LocationModel({ ...baseExamination.location });
  const patient = new PatientModel({ ...baseExamination.patient });

    const examination = new ExaminationModel({
      outcome: baseExamination.outcome,
      date: baseExamination.date,
      location,
      patient,
    });

    return Promise.all([
      location.save(),
      patient.save(),
    ]).then(() => examination.save());
};

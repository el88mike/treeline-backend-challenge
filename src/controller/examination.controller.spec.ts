import request from 'supertest';

import {
  DbMock,
  testExaminationPayload,
} from '../common/testing';

import { App } from '../app';

import {
  Examination,
} from '../model';

import { ExaminationController } from './examination.controller';

const PORT = 4001;

class ExaminationServiceMock {
  public saveExamination(payload: Partial<Examination>) {
    return Promise.resolve(payload);
  }
}

describe('ExaminationController', () => {
  const db = new DbMock();
  let app: App;
  let server: any;
  let controller: ExaminationController;

  beforeAll(async () => {
    app = new App();
    server = await app.run(PORT);
  });

  beforeEach(async () => await db.connect());
  afterEach(async () => await db.clear());

  afterAll(async () => {
    await server.close();
    async () => await db.close();
  });

  describe('POST /', () => {
    it('Accepts payload and return 200 when correct', done => {
      controller = new ExaminationController(new ExaminationServiceMock() as any);

      request(app.instance)
        .post('/examinations')
        .send(testExaminationPayload)
        .expect(200, done);
    });
  });

  it('Accepts payload and return 400 when incorrect', done => {
    controller = new ExaminationController(new ExaminationServiceMock() as any);

    request(app.instance)
      .post('/examinations')
      .expect(400, done);
  });
});

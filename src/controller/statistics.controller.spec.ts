import request from 'supertest';

import {
  DbMock,
  testExaminationPayload,
  saveExamination,
} from '../common/testing';

import { ExaminationOutcome } from '../model';

import { App } from '../app';

import { StatisticsController } from './statistics.controller';

const PORT = 4002;

const testWrongDay = new Date('2021-07-06T20:40:25Z');

describe('StatisticsController', () => {
  const db = new DbMock();
  let app: App;
  let server: any;
  let controller: StatisticsController;

  beforeAll(async () => {
    app = new App();
    server = await app.run(PORT);
  });

  beforeEach(async () => await db.connect());
  afterEach(async () => await db.clear());

  afterAll(async () => {
    await server.close();
    async () => await db.close();
  });

  describe('GET /allByDay', () => {
    it('Accepts query param and returns 200 when correct, alongside correct data', async done => {
      controller = new StatisticsController();

      await Promise.all([
        saveExamination(),
        saveExamination(),
        saveExamination({ date: testWrongDay }),
      ]);

      request(app.instance)
        .get(`/statistics/allByDay?day=${testExaminationPayload.date.toISOString()}`)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          expect(res.body?.length).toEqual(2);
          done();
        });
    });
  });

  describe('GET /allPositiveByDay', () => {
    it('Accepts query param and returns 200 when correct, alongside correct data', async done => {
      controller = new StatisticsController();

      await Promise.all([
        saveExamination(),
        saveExamination({ outcome: ExaminationOutcome.POSITIVE }),
        saveExamination({ date: testWrongDay }),
      ]);

      request(app.instance)
        .get(`/statistics/positiveByDay?day=${testExaminationPayload.date.toISOString()}`)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          expect(res.body?.length).toEqual(1);
          done();
        });
    });
  });

  describe('GET /allByCountry', () => {
    it('Accepts query param and returns 200 when correct, alongside correct data', async done => {
      controller = new StatisticsController();

      const testCountry = 'england';
      const testLocation = {
        latitude: 100,
        longitude: 200,
        country:  testCountry,
      };

      await Promise.all([
        saveExamination(),
        saveExamination(),
        saveExamination({ location: testLocation }),
      ]);

      request(app.instance)
        .get(`/statistics/allByCountry?country=${testCountry}`)
        .expect(200)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          expect(res.body?.length).toEqual(1);
          done();
        });
    });
  });
});

import {
  Request,
  Response,
  Router,
} from 'express';

import { StatisticsService } from '../service';

import { isValidationError } from '../error';

export class StatisticsController {
  constructor(
    private readonly statisticsService: StatisticsService = new StatisticsService(),
  ) {}

  public getRouter() {
    const router = Router();
  
    router.get('/allByDay', (req, res) => this.getAllExaminationsPerDay(req, res));
    router.get('/positiveByDay', (req, res) => this.getPositiveExaminationsPerDay(req, res));
    router.get('/allByCountry', (req, res) => this.getAllExaminationsByCountry(req, res));

    return router;
  }

  public async getAllExaminationsPerDay(request: Request, response: Response) {
    const day = new Date(request.query?.day as string || '');
  
    try {
      const examinations = await this.statisticsService.getAllByDay(day);

      response.status(200).send(examinations);
      
      return;
    } catch (error) {
      if (isValidationError(error)) {
        response.status(400).send(error.errors);
        return;
      }

      response.status(500).send('Something went wrong. Please try again later.');
      return;
    }
  }

  public async getPositiveExaminationsPerDay(request: Request, response: Response) {
    const day = new Date(request.query?.day as string || '');
  
    try {
      const examinations = await this.statisticsService.getPositiveByDay(day);

      response.status(200).send(examinations);
      
      return;
    } catch (error) {
      if (isValidationError(error)) {
        response.status(400).send(error.errors);
        return;
      }

      response.status(500).send('Something went wrong. Please try again later.');
      return;
    }
  }

  public async getAllExaminationsByCountry(request: Request, response: Response) {
    const country = request.query.country as string || '';
  
    try {
      const examinations = await this.statisticsService.getAllByCountry(country);

      response.status(200).send(examinations);
      
      return;
    } catch (error) {
      if (isValidationError(error)) {
        response.status(400).send(error.errors);
        return;
      }

      response.status(500).send('Something went wrong. Please try again later.');
      return;
    }
  }
}

import {
  Request,
  Response,
  Router,
} from 'express';

import { ExaminationService } from '../service';

import { isValidationError } from '../error';

export class ExaminationController {
  constructor(
    private readonly examinationService: ExaminationService = new ExaminationService(),
  ) {}

  public getRouter() {
    const router = Router();
  
    router.post('/', (req, res) => this.saveExamination(req, res));

    return router;
  }

  public async saveExamination(request: Request, response: Response) {
    const payload = request.body;

    try {
      const examination = await this.examinationService.saveExamination(payload);

      response.status(200).send(examination);
      
      return;
    } catch (error) {
      if (isValidationError(error)) {
        response.status(400).send(error.errors);
        return;
      }

      response.status(500).send('Something went wrong. Please try again later.');
      return;
    }
  }
}

import request from 'supertest';

import { App } from '../app';

const PORT = 4001;

describe('HelloController', () => {
  let app: App;
  let server: any;

  beforeAll(async () => {
    app = new App();
    server = await app.run(PORT);
  });

  afterAll(async () => {
    await server.close();
  });

  it('returns 200 on GET /', (done) => {
    request(app.instance).get('/').expect(200, done);
  });
});

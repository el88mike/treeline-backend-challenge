import { config as dotenvConfig } from 'dotenv';

export enum ConfigProperty {
  PORT = 'PORT',
}

export interface AppConfig {
  port: number;
}

export class ConfigService {
  public createConfig() {
    dotenvConfig();

    return {
      port: Number(process.env[ConfigProperty.PORT]),
    } as AppConfig;
  };
}
